#include "roman_number_format_error.h"

const char * roman_number_format_error::what() const noexcept {
    return "Unnable to parse symbol as roman number";
}
