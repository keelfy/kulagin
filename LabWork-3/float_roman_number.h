#ifndef FLOAT_ROMAN_NUMBER_H
#define FLOAT_ROMAN_NUMBER_H

#include <iostream>
#include "roman_number.h"

class float_roman_number : public roman_number
{
public:
    float_roman_number();
    float_roman_number(double);
    float_roman_number(std::string&);
    float_roman_number(roman_number*);

    void from(double number) override;

    roman_number& operator+=(double) override;

    roman_number& operator*=(double) override;

    roman_number& operator/=(double) override;

    bool operator==(double) override;

    double to_double() override;

    double calculate_arabic(std::string&) override;

    std::string calculate_roman(double) override;

    ~float_roman_number() override;
};

#endif
