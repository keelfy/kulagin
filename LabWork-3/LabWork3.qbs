import qbs

CppApplication {
    Properties {
        cpp.cxxLanguageVersion: "c++11"
    }

    consoleApplication: true
    files: [
        "float_roman_number.cpp",
        "float_roman_number.h",
        "int_roman_number.cpp",
        "int_roman_number.h",
        "main.cpp",
        "number_format_error.cpp",
        "number_format_error.h",
        "roman_number.cpp",
        "roman_number.h",
        "roman_number_format_error.cpp",
        "roman_number_format_error.h",
    ]

    Group {     // Properties for the produced executable
        fileTagsFilter: "application"
        qbs.install: true
        qbs.installDir: "bin"
    }
}
