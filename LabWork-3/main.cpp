#include <iostream>
#include <random>
#include <time.h>

#include "roman_number.h"
#include "int_roman_number.h"
#include "float_roman_number.h"

double sum(roman_number* numbers, unsigned int size) {
    double result = 0;

    for (unsigned int i = 0; i < size; i++) {
        result += numbers[i].to_double();
    }

    return result;
};

int main()
{
    srand(time_t(0));

    unsigned int const size = 10;
    roman_number* numbers = new int_roman_number[size];

    std::cout << "-- Numbers:" << std::endl;
    for (unsigned int i = 0; i < size; i++) {
        numbers[i] = int_roman_number(rand() % 1000000 + 1);
        std::cout << "#" << i + 1 << " = " << numbers[i] << std::endl;
    }

    std::cout << "-- Result => " << sum(numbers, size) << std::endl;

    delete [] numbers;
    return 0;
}
