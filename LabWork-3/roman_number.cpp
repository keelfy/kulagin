#include "roman_number.h"
#include "number_format_error.h"
#include "roman_number_format_error.h"

std::string roman_number::ones[10] = {"", "i", "ii", "iii", "iv", "v", "vi", "vii", "viii", "ix"};
std::string roman_number::tens[10] = {"", "x", "xx", "xxx", "xl", "l", "lx", "lxx", "lxxx", "xc"};
std::string roman_number::hunds[10] = {"", "c", "cc", "ccc", "cd", "d", "dc", "dcc", "dccc", "cm"};
std::string roman_number::thous[10] = {"", "m", "mm", "mmm", "mV", "V", "Vm", "Vmm", "Vmmm", "mX"};
std::string roman_number::thous_ten[10] = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
std::string roman_number::thous_hunds[10] = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
std::string roman_number::mill[10] = {"", "M", "MM", "MMM", "MMMM"};
std::map<char, unsigned int> roman_number::values = {
    {'M', 1000000},
    {'D', 500000},
    {'C', 100000},
    {'L', 50000},
    {'X', 10000},
    {'V', 5000},
    {'m', 1000},
    {'d', 500},
    {'c', 100},
    {'l', 50},
    {'x', 10},
    {'v', 5},
    {'i', 1}
};

roman_number::roman_number() {
    this->arabic = 0;
    this->roman = "";
};

roman_number::roman_number(double) {};

roman_number::roman_number(std::string& number) {
    this->roman_number::from(number);
}

roman_number::roman_number(roman_number* number) {
    roman_number::from(*number);
};

roman_number::~roman_number() {};

bool roman_number::check_roman_symbols(std::string& symbols) {
    std::map<char, unsigned int>::iterator it;
    char ch;

    for (unsigned int i = 0; i < symbols.length(); i++) {
        ch = symbols.at(i);

        it = values.find(ch);
        if(it != values.end()) {
            continue;
        }
        throw roman_number_format_error();
    }
    return true;
};

void roman_number::from(std::string& number) {
    this->roman = number;
    this->arabic = calculate_arabic(roman);
};

void roman_number::from(roman_number& number) {
    this->roman = number.to_string();
    this->arabic = number.to_double();
}

roman_number& roman_number::add(roman_number& number) {
    this->arabic += number.to_double();
    this->roman = calculate_roman(arabic);
    return *this;
};

roman_number& roman_number::subtract(roman_number& number) {
    this->arabic -= number.to_double();
    this->roman = calculate_roman(arabic);
    return *this;
};

roman_number& roman_number::multiply(roman_number& number) {
    this->arabic *= number.to_double();
    this->roman = calculate_roman(arabic);
    return *this;
};

roman_number& roman_number::divide(roman_number& number) {
    this->arabic /= number.to_double();
    this->roman = calculate_roman(arabic);
    return *this;
};

roman_number& roman_number::operator=(std::string b) {
    this->roman = b;
    this->arabic = calculate_arabic(roman);
    return *this;
};

roman_number& roman_number::operator=(double b) {
    this->from(b);
    return *this;
};

roman_number& roman_number::operator+=(roman_number& b) {
    return this->operator+=(b.to_double());
};

roman_number& roman_number::operator-=(roman_number& b) {
    return this->operator-=(b.to_double());
};

roman_number& roman_number::operator-=(double b) {
    return this->operator+=(-b);
};

roman_number& roman_number::operator*=(roman_number& b) {
    return this->operator*=(b.to_double());
};

roman_number& roman_number::operator/=(roman_number& b) {
    return this->operator/=(b.to_double());
};

bool roman_number::operator==(roman_number& b) {
    return this->operator==(b.to_double());
};

bool roman_number::operator!=(roman_number& b) {
    return this->operator!=(b.to_double());
};

bool roman_number::operator!=(double b) {
    return !this->operator==(b);
};

bool roman_number::operator<(roman_number& b) {
    return this->operator<(b.to_double());
};

bool roman_number::operator<(double b) {
    return this->arabic < b;
};

bool roman_number::operator>(roman_number& b) {
    return this->operator>(b.to_double());
};

bool roman_number::operator>(double b) {
    return this->arabic > b;
};

bool roman_number::operator<=(roman_number& b) {
    return this->operator<=(b.to_double());
};

bool roman_number::operator<=(double b) {
    return this->arabic <= b;
};

bool roman_number::operator>=(roman_number& b) {
    return this->operator>=(b.to_double());
};

bool roman_number::operator>=(double b) {
    return this->arabic >= b;
};

std::string& roman_number::to_string() {
    return roman;
};

std::ostream& operator<<(std::ostream& os, roman_number& number) {
    os << number.to_string() << " (" << number.to_double() << ")";
    return os;
};

std::istream& operator>>(std::istream& is, roman_number& number) {
    unsigned int value;
    std::string romanInput;
    double arabicInput;

    std::cout << "Do you want to enter arabic or roman value?\n  Type 1 or 2 => ";
    std::getline(is, romanInput);

    if(romanInput.empty() || !is || is.eof() || romanInput.find_first_not_of("0123456789") != std::string::npos) {
        throw number_format_error();
    }

    value = std::stoul(romanInput);

    if (value == 1) {
        std::cout << "Type arabic value => ";
        std::getline(is, romanInput);

        if(romanInput.empty() || !is || is.eof() || romanInput.find_first_not_of("0123456789") != std::string::npos) {
            throw number_format_error();
        }

        arabicInput = std::stoul(romanInput);
        number.from(arabicInput);
    } else {
        std::cout << "Type roman value => ";
        std::getline(is, romanInput);

        if (romanInput.empty() || !is || is.eof()) {
            throw roman_number_format_error();
        }

        number.check_roman_symbols(romanInput);
        number.from(romanInput);
    }
    return is;
};
