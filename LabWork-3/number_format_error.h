#ifndef NUMBER_FORMAT_ERROR_H
#define NUMBER_FORMAT_ERROR_H

#include <iostream>

class number_format_error : public std::exception {
public:
    const char * what() const noexcept;
};

#endif
