#include "float_roman_number.h"
#include <vector>

float_roman_number::float_roman_number() : roman_number() {};
float_roman_number::float_roman_number(std::string& number) : roman_number(number) {};
float_roman_number::float_roman_number(roman_number* number) : roman_number(number) {};

float_roman_number::float_roman_number(double number) : roman_number(number) {
    this->from(number);
};

float_roman_number::~float_roman_number() {}

void float_roman_number::from(double number) {
     this->arabic = number;
     this->roman = calculate_roman(arabic);
};

roman_number& float_roman_number::operator+=(double b) {
    this->arabic += b;
    this->roman = calculate_roman(arabic);
    return *this;
};

roman_number& float_roman_number::operator*=(double b) {
    this->arabic *= b;
    this->roman = calculate_roman(arabic);
    return *this;
};

roman_number& float_roman_number::operator/=(double b) {
    this->arabic /= b;
    this->roman = calculate_roman(arabic);
    return *this;
};

bool float_roman_number::operator==(double b) {
    std::string buffer = calculate_roman(b);
    return this->roman == buffer;
};

double float_roman_number::calculate_arabic(std::string& number) {
    if (number.length() == 0)
        return 0;

    std::vector<unsigned int> numbers;
    for (unsigned int i = 0; i < number.length(); i++) {
        numbers.push_back(values[number.at(i)]);
    }

    unsigned int result = 0;
    unsigned int num1 = 0;
    unsigned int num2 = 0;

    for (unsigned int i = 0; i < numbers.size() - 1; i++) {
        num1 = numbers.at(i);
        num2 = numbers.at(i + 1);
        if (num1 >= num2) {
            result += num1;
        } else {
            result -= num2;
        }
    }
    result += numbers.at(numbers.size() - 1);
    return result;
};

std::string float_roman_number::calculate_roman(double number) {
    if (number <= 0)
        return "";

    int int_number = static_cast<int>(number);

    std::string result = "";
    std::string m = mill[int_number / 1000000];
    std::string th = thous_hunds[int_number / 100000 % 10];
    std::string tt = thous_ten[int_number / 10000 % 10];
    std::string t = thous[int_number / 1000 % 10];
    std::string h = hunds[int_number / 100 % 10];
    std::string te = tens[int_number / 10 % 10];
    std::string o = ones[int_number % 10];
    result = m + th + tt + t + h + te + o;
    return result;
};

double float_roman_number::to_double() {
    return this->arabic;
};
