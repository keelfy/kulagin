#ifndef ROMAN_NUMBER_H
#define ROMAN_NUMBER_H

#include <iostream>
#include <map>

class roman_number
{
protected:
    std::string roman;
    double arabic;

public:
    static std::string ones[10];
    static std::string tens[10];
    static std::string hunds[10];
    static std::string thous[10];
    static std::string thous_ten[10];
    static std::string thous_hunds[10];
    static std::string mill[10];
    static std::map<char, unsigned int> values;

    roman_number();
    roman_number(double);
    roman_number(std::string&);
    roman_number(roman_number*);

    virtual ~roman_number() = 0;

    void from(roman_number&);

    virtual void from(double) = 0;

    void from(std::string&);

    roman_number& add(roman_number&);

    roman_number& subtract(roman_number&);

    roman_number& multiply(roman_number&);

    roman_number& divide(roman_number&);

    roman_number& operator=(std::string);

    roman_number& operator=(double);

    roman_number& operator+=(roman_number&);

    virtual roman_number& operator+=(double) = 0;

    roman_number& operator-=(roman_number&);

    roman_number& operator-=(double);

    roman_number& operator*=(roman_number&);

    virtual roman_number& operator*=(double) = 0;

    roman_number& operator/=(roman_number&);

    virtual roman_number& operator/=(double) = 0;

    bool operator==(roman_number&);

    virtual bool operator==(double) = 0;

    bool operator!=(roman_number&);

    bool operator!=(double);

    bool operator<(roman_number&);

    bool operator<(double);

    bool operator>(roman_number&);

    bool operator>(double);

    bool operator<=(roman_number&);

    bool operator<=(double);

    bool operator>=(roman_number&);

    bool operator>=(double);

    std::string& to_string();

    virtual double to_double() = 0;

    virtual double calculate_arabic(std::string&) = 0;

    virtual std::string calculate_roman(double) = 0;

    bool check_roman_symbols(std::string&);

    friend std::ostream& operator<<(std::ostream&, roman_number&);
    friend std::istream& operator>>(std::istream&, roman_number&);
};

#endif
