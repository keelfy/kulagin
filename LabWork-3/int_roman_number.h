#ifndef INT_ROMAN_NUMBER_H
#define INT_ROMAN_NUMBER_H

#include <iostream>
#include "roman_number.h"

class int_roman_number : public roman_number
{
public:
    int_roman_number();
    int_roman_number(double);
    int_roman_number(std::string&);
    int_roman_number(roman_number*);

    void from(double) override;

    roman_number& operator+=(double) override;

    roman_number& operator*=(double) override;

    roman_number& operator/=(double) override;

    bool operator==(double) override;

    double to_double() override;

    double calculate_arabic(std::string&) override;

    std::string calculate_roman(double) override;

    ~int_roman_number() override;
};

#endif
