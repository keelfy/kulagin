#ifndef ROMAN_NUMBER_H
#define ROMAN_NUMBER_H

#include <iostream>
#include <map>

class roman_number {
    std::string roman;
    unsigned int arabic;

public:
    static std::string ones[10];
    static std::string tens[10];
    static std::string hunds[10];
    static std::string thous[10];
    static std::string thous_ten[10];
    static std::string thous_hunds[10];
    static std::string mill[10];
    static std::map<char, unsigned int> values;

    static bool check_roman_symbols(std::string&);

    roman_number(std::string = "");
    roman_number(unsigned long = 0);
    roman_number(roman_number*);

    void from(roman_number&);
    void from(std::string&);
    void from(unsigned int);

    roman_number& add(roman_number&);
    roman_number& subtract(roman_number&);
    roman_number& multiply(roman_number&);
    roman_number& divide(roman_number&);

    roman_number& operator=(std::string);
    roman_number& operator=(unsigned int);
    roman_number& operator+=(roman_number&);
    roman_number& operator+=(unsigned int);
    roman_number& operator-=(roman_number&);
    roman_number& operator-=(unsigned int);
    roman_number& operator*=(roman_number&);
    roman_number& operator*=(unsigned int);
    roman_number& operator/=(roman_number&);
    roman_number& operator/=(unsigned int);

    roman_number operator++();
    roman_number operator++(int);
    roman_number operator--();
    roman_number operator--(int);

    bool operator==(roman_number&);
    bool operator==(unsigned int);
    bool operator!=(roman_number&);
    bool operator!=(unsigned int);
    bool operator<(roman_number&);
    bool operator<(unsigned int);
    bool operator>(roman_number&);
    bool operator>(unsigned int);
    bool operator<=(roman_number&);
    bool operator<=(unsigned int);
    bool operator>=(roman_number&);
    bool operator>=(unsigned int);

    std::string& get_roman();
    unsigned int get_arabic();

    unsigned int calculate_arabic();
    std::string calculate_roman();

    friend std::ostream& operator<<(std::ostream&, roman_number&);
    friend std::istream& operator>>(std::istream&, roman_number&);
    friend roman_number operator+(roman_number&, roman_number&);
    friend roman_number operator+(roman_number&, unsigned int);
    friend roman_number operator-(roman_number&, roman_number&);
    friend roman_number operator-(roman_number&, unsigned int);
    friend roman_number operator*(roman_number&, roman_number&);
    friend roman_number operator*(roman_number&, unsigned int);
    friend roman_number operator/(roman_number&, roman_number&);
    friend roman_number operator/(roman_number&, unsigned int);
};

#endif
