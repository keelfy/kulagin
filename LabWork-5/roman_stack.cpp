#include "roman_stack.h"

roman_stack::roman_stack() {
    this->stack = std::stack<roman_number>();
};

roman_stack::roman_stack(roman_stack& value) {
    this->stack = value.stack;
};

void roman_stack::push(roman_number& number) {
    this->stack.push(number);
};

void roman_stack::push(unsigned int number) {
    this->stack.push(roman_number(number));
};

void roman_stack::push(std::string& number) {
    if (roman_number::check_roman_symbols(number)) {
        this->stack.push(roman_number(number));
    }
};

void roman_stack::pop() {
    this->stack.pop();
};

roman_number& roman_stack::top() {
    return this->stack.top();
};

unsigned int roman_stack::size() {
    return this->stack.size();
};

bool roman_stack::empty() {
    return this->stack.empty();
};
