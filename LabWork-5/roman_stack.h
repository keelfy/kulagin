#ifndef ROMAN_STACK_H
#define ROMAN_STACK_H

#include "roman_number.h"
#include <stack>

class roman_stack
{
    std::stack<roman_number> stack;

public:
    roman_stack();
    roman_stack(roman_stack&);

    void push(roman_number&);
    void push(unsigned int);
    void push(std::string&);

    void pop();

    roman_number& top();

    unsigned int size();

    bool empty();

};

#endif
