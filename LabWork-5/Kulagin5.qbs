import qbs

CppApplication {
    Properties {
        cpp.cxxLanguageVersion: "c++11"
    }

    consoleApplication: true
    files: [
        "main.cpp",
        "number_format_exception.cpp",
        "number_format_exception.h",
        "roman_number.cpp",
        "roman_number.h",
        "roman_number_format_exception.cpp",
        "roman_number_format_exception.h",
        "roman_stack.cpp",
        "roman_stack.h",
    ]

    Group {     // Properties for the produced executable
        fileTagsFilter: "application"
        qbs.install: true
        qbs.installDir: "bin"
    }
}
