#include <iostream>
#include <fstream>
#include <sstream>
#include "roman_stack.h"

/// Путь до лог-файла
static std::string LOG_PATH = "C:\\launch.log";

/// Выводит строку и в консоль, и в лог
void log(std::ostream& console, std::ofstream& log_file, std::string message) {
    console << message;
    log_file << message;
};

/// Запрашивает количество чисел и вводит их в стэк, после возвращая его
roman_stack input_numbers(std::istream& is, std::ostream& os, std::ofstream& log_file) {
    roman_stack stack;
    roman_number number("");
    unsigned int amount;

    log(os, log_file, "-- Amount of numbers => ");
    is >> amount;
    is.ignore();
    log_file << amount << std::endl;

    log(os, log_file, "-- Enter numbers: \n");

    for (unsigned int i = 0; i < amount; i++) {
        is >> number;
        log_file << number << std::endl;
        stack.push(number);
    }

    return stack;
};

/// Суммирует значения в стэке и записывает результат в конец стэка
void sum_up(roman_stack& stack) {
    roman_stack buffer(stack);
    unsigned int sum = 0;

    for (unsigned int i = 0; i < stack.size(); i++) {
        sum += buffer.top().get_arabic();
        buffer.pop();
    }

    stack.push(sum);
};

/// Делит значения в стэке и записывает результат в конец стэка
void divide(roman_stack& stack) {
    if (stack.empty()) {
        stack.push(1);
        return;
    }

    roman_stack buffer(stack);
    unsigned int sum = buffer.top().get_arabic();

    for (unsigned int i = 1; i < stack.size(); i++) {
        sum /= buffer.top().get_arabic();
        buffer.pop();
    }

    stack.push(sum);
};

/// Перемножает значения в стэке и записывает результат в конец стэка
void multiply(roman_stack& stack) {
    roman_stack buffer(stack);
    unsigned int sum = 1;

    for (unsigned int i = 0; i < stack.size(); i++) {
        sum *= buffer.top().get_arabic();
        buffer.pop();
    }

    stack.push(sum);
};

/// Вычитает значения в стэке и записывает результат в конец стэка. Если результат будет < 0, то запишется 0
void subtract(roman_stack& stack) {
    roman_stack buffer(stack);
    unsigned int sum = 1;

    for (unsigned int i = 0; i < stack.size(); i++) {
        sum -= buffer.top().get_arabic();
        buffer.pop();
    }

    stack.push(sum > 1 ? sum : 1);
};

int main()
{
    std::ofstream log_file(LOG_PATH); // Лог-файл

    bool terminated = false;
    unsigned int action;
    roman_stack stack;
    std::stringstream ss;

    while (!terminated) {
        log(std::cout, log_file, "What do you want to do?\n1. Sum up\n2. Divide\n3. Multiply\n4. Subtract\n5. Exit from program\n => ");
        std::cin >> action;
        log_file << action << std::endl;

        if (action < 1 || action > 4) {
            terminated = true;
            continue;
        }

        stack = input_numbers(std::cin, std::cout, log_file);

        switch (action) {
        case 1:
            sum_up(stack);
            break;
        case 2:
            divide(stack);
            break;
        case 3:
            multiply(stack);
            break;
        case 4:
            subtract(stack);
            break;
        }

        ss << "Result => " << stack.top() << std::endl << std::endl;
        log(std::cout, log_file, ss.str());
        ss.clear();
    }

    log(std::cout, log_file, "--- Terminated ---\n");
    log_file.close();

    return 0;
};
