#include "roman_number_format_exception.h"

const char * roman_number_format_exception::what() const noexcept {
    return "Unnable to parse symbol as roman number";
};
