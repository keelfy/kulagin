#include "roman_number.h"
#include "roman_number_format_exception.h"
#include "number_format_exception.h"
#include <vector>

std::string roman_number::ones[10] = {"", "i", "ii", "iii", "iv", "v", "vi", "vii", "viii", "ix"};
std::string roman_number::tens[10] = {"", "x", "xx", "xxx", "xl", "l", "lx", "lxx", "lxxx", "xc"};
std::string roman_number::hunds[10] = {"", "c", "cc", "ccc", "cd", "d", "dc", "dcc", "dccc", "cm"};
std::string roman_number::thous[10] = {"", "m", "mm", "mmm", "mV", "V", "Vm", "Vmm", "Vmmm", "mX"};
std::string roman_number::thous_ten[10] = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
std::string roman_number::thous_hunds[10] = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
std::string roman_number::mill[10] = {"", "M", "MM", "MMM", "MMMM"};
std::map<char, unsigned int> roman_number::values = {
    {'M', 1000000},
    {'D', 500000},
    {'C', 100000},
    {'L', 50000},
    {'X', 10000},
    {'V', 5000},
    {'m', 1000},
    {'d', 500},
    {'c', 100},
    {'l', 50},
    {'x', 10},
    {'v', 5},
    {'i', 1}
};

/** @brief Создает римское число из строки и сразу высчитывает арабское */
roman_number::roman_number(std::string number) {
    this->roman = number;
    this->arabic = calculate_arabic();
};

/** @brief Создает римское число из арабского */
roman_number::roman_number(unsigned long number) {
    this->arabic = number;
    this->roman = calculate_roman();
};

/** @brief Конструктор копирования */
roman_number::roman_number(roman_number* number) {
    this->from(*number);
};

/** @brief Копирует всю информацию из переданного римского числа */
void roman_number::from(roman_number& number) {
    this->arabic = number.get_arabic();
    this->roman = number.get_roman();
};

void roman_number::from(std::string& number) {
    if (check_roman_symbols(number)) {
        this->roman = number;
        this->arabic = calculate_arabic();
    }
};

void roman_number::from(unsigned int number) {
    this->arabic = number;
    this->roman = calculate_roman();
};

/** @brief Сумма двух римских чисел */
roman_number& roman_number::add(roman_number& number) {
    this->arabic += number.get_arabic();
    this->roman = calculate_roman();
    return *this;
};

/** @brief Разница двух римских чисел */
roman_number& roman_number::subtract(roman_number& number) {
    this->arabic -= number.get_arabic();
    this->roman = calculate_roman();
    return *this;
};

/** @brief Произведение двух римских чисел */
roman_number& roman_number::multiply(roman_number& number) {
    this->arabic *= number.get_arabic();
    this->roman = calculate_roman();
    return *this;
};

/** @brief Частное двух римских чисел */
roman_number& roman_number::divide(roman_number& number) {
    this->arabic /= number.get_arabic();
    this->roman = calculate_roman();
    return *this;
};

roman_number& roman_number::operator=(std::string b) {
    this->roman = b;
    this->arabic = calculate_arabic();
    return *this;
};

roman_number& roman_number::operator=(unsigned int b) {
    this->arabic = b;
    this->roman = calculate_roman();
    return *this;
};

roman_number& roman_number::operator+=(roman_number& b) {
    return this->operator+=(b.get_arabic());
};

roman_number& roman_number::operator+=(unsigned int b) {
    this->arabic += b;
    this->roman = calculate_roman();
    return *this;
};

roman_number& roman_number::operator-=(roman_number& b) {
    return this->operator-=(b.get_arabic());
};

roman_number& roman_number::operator-=(unsigned int b) {
    return this->operator+=(-b);
};

roman_number& roman_number::operator*=(roman_number& b) {
    return this->operator*=(b.get_arabic());
};

roman_number& roman_number::operator*=(unsigned int b) {
    this->arabic *= b;
    this->roman = calculate_roman();
    return *this;
};

roman_number& roman_number::operator/=(roman_number& b) {
    return this->operator/=(b.get_arabic());
};

roman_number& roman_number::operator/=(unsigned int b) {
    this->arabic /= b;
    this->roman = calculate_roman();
    return *this;
};

roman_number roman_number::operator++() {
    return this->operator+=(1);
};

roman_number roman_number::operator++(int) {
    roman_number number(this);
    this->operator+=(1);
    return number;
};

roman_number roman_number::operator--() {
    return this->operator-=(1);
};

roman_number roman_number::operator--(int) {
    roman_number number(this);
    this->operator-=(1);
    return number;
};

bool roman_number::operator==(roman_number& b) {
    return this->operator==(b.get_arabic());
};

bool roman_number::operator==(unsigned int b) {
    return this->arabic == b;
};

bool roman_number::operator!=(roman_number& b) {
    return this->operator!=(b.get_arabic());
};

bool roman_number::operator!=(unsigned int b) {
    return !this->operator==(b);
};

bool roman_number::operator<(roman_number& b) {
    return this->operator<(b.get_arabic());
};

bool roman_number::operator<(unsigned int b) {
    return this->arabic < b;
};

bool roman_number::operator>(roman_number& b) {
    return this->operator>(b.get_arabic());
};

bool roman_number::operator>(unsigned int b) {
    return this->arabic > b;
};

bool roman_number::operator<=(roman_number& b) {
    return this->operator<=(b.get_arabic());
};

bool roman_number::operator<=(unsigned int b) {
    return this->arabic <= b;
};

bool roman_number::operator>=(roman_number& b) {
    return this->operator>=(b.get_arabic());
};

bool roman_number::operator>=(unsigned int b) {
    return this->arabic >= b;
};

std::string& roman_number::get_roman() {
    return roman;
};

unsigned int roman_number::get_arabic() {
    return arabic;
};

/**
        @brief Переводит римское число в арабское
        @param number - Римское число
        @return Арабское число
    */
unsigned int roman_number::calculate_arabic() {
    if (roman.length() == 0)
        return 0;

    std::vector<unsigned int> numbers;
    for (unsigned int i = 0; i < roman.length(); i++) {
        numbers.push_back(values[roman.at(i)]);
    }

    unsigned int result = 0;
    unsigned int num1 = 0;
    unsigned int num2 = 0;

    for (unsigned int i = 0; i < numbers.size() - 1; i++) {
        num1 = numbers.at(i);
        num2 = numbers.at(i + 1);
        if (num1 >= num2) {
            result += num1;
        } else {
            result -= num2;
        }
    }
    result += numbers.at(numbers.size() - 1);
    return result;
};

/**
        @brief Переводит арабское число в римское
        @param number - Арабское число
        @return Римское число
    */
std::string roman_number::calculate_roman() {
    if (arabic <= 0)
        return "";

    std::string result = "";
    std::string m = mill[arabic / 1000000];
    std::string th = thous_hunds[arabic / 100000 % 10];
    std::string tt = thous_ten[arabic / 10000 % 10];
    std::string t = thous[arabic / 1000 % 10];
    std::string h = hunds[arabic / 100 % 10];
    std::string te = tens[arabic / 10 % 10];
    std::string o = ones[arabic % 10];
    result = m + th + tt + t + h + te + o;
    return result;
};

/**
    @brief Проверяет символы на их "подходимость" под римские числа
    @throws выбрасывает исключение, если какой-то символ не подходит
    @returns возвращает true, если всё корректно
*/
bool roman_number::check_roman_symbols(std::string& symbols) {
    std::map<char, unsigned int>::iterator it;
    char ch;

    for (unsigned int i = 0; i < symbols.length(); i++) {
        ch = symbols.at(i);

        it = values.find(ch);
        if(it != values.end()) {
            continue;
        }
        throw roman_number_format_exception();
    }
    return true;
};

roman_number operator+(roman_number& a, roman_number& b) {
    return a + b.get_arabic();
};

roman_number operator+(roman_number& a, unsigned int b) {
    return roman_number(a.get_arabic() + b);
};

roman_number operator-(roman_number& a, roman_number& b) {
    return a - b.get_arabic();
};

roman_number operator-(roman_number& a, unsigned int b) {
    return roman_number(a.get_arabic() - b);
};

roman_number operator*(roman_number& a, roman_number& b) {
    return a * b.get_arabic();
};

roman_number operator*(roman_number& a, unsigned int b) {
    return roman_number(a.get_arabic() * b);
};

roman_number operator/(roman_number& a, roman_number& b) {
    return a / b.get_arabic();
};

roman_number operator/(roman_number& a, unsigned int b) {
    return roman_number(a.get_arabic() / b);
};

std::ostream& operator<<(std::ostream& os, roman_number& number) {
    os << number.get_roman() << " (" << number.get_arabic() << ")";
    return os;
};

std::istream& operator>>(std::istream& is, roman_number& number) {
    unsigned int value;
    roman_number temp("");
    std::string input;

    std::cout << "Do you want to enter arabic or roman value?\n  Type 1 or 2 => ";
    std::getline(is, input);

    if(input.empty() || !is || is.eof() || input.find_first_not_of("0123456789") != std::string::npos) {
        throw number_format_exception();
    }

    value = std::stoul(input);

    if (value == 1) {
        std::cout << "Type arabic value => ";
        std::getline(is, input);

        if(input.empty() || !is || is.eof() || input.find_first_not_of("0123456789") != std::string::npos) {
            throw number_format_exception();
        }

        temp = roman_number(std::stoul(input));
    } else {
        std::cout << "Type roman value => ";
        std::getline(is, input);

        if (input.empty() || !is || is.eof()) {
            throw roman_number_format_exception();
        }

        number.check_roman_symbols(input);
        temp = roman_number(input);
    }
    number.from(temp);
    return is;
};
