#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <string>

static std::string ones[10] = {"", "i", "ii", "iii", "iv", "v", "vi", "vii", "viii", "ix"};
static std::string tens[10] = {"", "x", "xx", "xxx", "xl", "l", "lx", "lxx", "lxxx", "xc"};
static std::string hunds[10] = {"", "c", "cc", "ccc", "cd", "d", "dc", "dcc", "dccc", "cm"};
static std::string thous[10] = {"", "m", "mm", "mmm", "mV", "V", "Vm", "Vmm", "Vmmm", "mX"};
static std::string thous_ten[10] = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
static std::string thous_hunds[10] = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
static std::string mill[10] = {"", "M", "MM", "MMM", "MMMM"};
static std::map<char, unsigned int> values;

/// Исключение, которое выбрасывается, если введены некорректные римские символы
class roman_number_format_error : public std::exception {
public:
    const char * what() const noexcept {
        return "Unnable to parse symbol as roman number";
    }
};

/// Исключение, которое выбрасывается, если введено некорректное число
class number_format_error : public std::exception {
public:
    const char * what() const noexcept {
        return "Unnable to parse number";
    }
};

/**
    @brief Проверяет символы на их "подходимость" под римские числа
    @throws выбрасывает исключение, если какой-то символ не подходит
    @returns возвращает true, если всё корректно
*/
bool check_roman_symbols(std::string& symbols) {
    std::map<char, unsigned int>::iterator it;
    char ch;

    for (unsigned int i = 0; i < symbols.length(); i++) {
        ch = symbols.at(i);

        it = values.find(ch);
        if(it != values.end()) {
            continue;
        }
        throw roman_number_format_error();
    }
    return true;
}

class roman_number {
    std::string roman;
    unsigned int arabic;

public:
    /** @brief Создает римское число из строки и сразу высчитывает арабское */
    roman_number(std::string number = "") {
        this->roman = number;
        this->arabic = calculate_arabic();
    }

    /** @brief Создает римское число из арабского */
    roman_number(unsigned long number = 0) {
        this->arabic = number;
        this->roman = calculate_roman();
    }

    /** @brief Конструктор копирования */
    roman_number(roman_number* number) {
        this->from(*number);
    }

    /** @brief Копирует всю информацию из переданного римского числа */
    void from(roman_number& number) {
         this->arabic = number.get_arabic();
         this->roman = number.get_roman();
     }

    /** @brief Сумма двух римских чисел */
    roman_number& add(roman_number& number) {
        this->arabic += number.get_arabic();
        this->roman = calculate_roman();
        return *this;
    }

    /** @brief Разница двух римских чисел */
    roman_number& subtract(roman_number& number) {
        this->arabic -= number.get_arabic();
        this->roman = calculate_roman();
        return *this;
    }

    /** @brief Произведение двух римских чисел */
    roman_number& multiply(roman_number& number) {
        this->arabic *= number.get_arabic();
        this->roman = calculate_roman();
        return *this;
    }

    /** @brief Частное двух римских чисел */
    roman_number& divide(roman_number& number) {
        this->arabic /= number.get_arabic();
        this->roman = calculate_roman();
        return *this;
    }

    roman_number& operator=(std::string b) {
        this->roman = b;
        this->arabic = calculate_arabic();
        return *this;
    }

    roman_number& operator=(unsigned int b) {
        this->arabic = b;
        this->roman = calculate_roman();
        return *this;
    }

    roman_number& operator+=(roman_number& b) {
        return this->operator+=(b.get_arabic());
    }

    roman_number& operator+=(unsigned int b) {
        this->arabic += b;
        this->roman = calculate_roman();
        return *this;
    }

    roman_number& operator-=(roman_number& b) {
        return this->operator-=(b.get_arabic());
    }

    roman_number& operator-=(unsigned int b) {
        return this->operator+=(-b);
    }

    roman_number& operator*=(roman_number& b) {
        return this->operator*=(b.get_arabic());
    }

    roman_number& operator*=(unsigned int b) {
        this->arabic *= b;
        this->roman = calculate_roman();
        return *this;
    }

    roman_number& operator/=(roman_number& b) {
        return this->operator/=(b.get_arabic());
    }

    roman_number& operator/=(unsigned int b) {
        this->arabic /= b;
        this->roman = calculate_roman();
        return *this;
    }

    roman_number operator++() {
        return this->operator+=(1);
    }

    roman_number operator++(int) {
        roman_number number(this);
        this->operator+=(1);
        return number;
    }

    roman_number operator--() {
        return this->operator-=(1);
    }

    roman_number operator--(int) {
        roman_number number(this);
        this->operator-=(1);
        return number;
    }

    bool operator==(roman_number& b) {
        return this->operator==(b.get_arabic());
    }

    bool operator==(unsigned int b) {
        return this->arabic == b;
    }

    bool operator!=(roman_number& b) {
        return this->operator!=(b.get_arabic());
    }

    bool operator!=(unsigned int b) {
        return !this->operator==(b);
    }

    bool operator<(roman_number& b) {
        return this->operator<(b.get_arabic());
    }

    bool operator<(unsigned int b) {
        return this->arabic < b;
    }

    bool operator>(roman_number& b) {
        return this->operator>(b.get_arabic());
    }

    bool operator>(unsigned int b) {
        return this->arabic > b;
    }

    bool operator<=(roman_number& b) {
        return this->operator<=(b.get_arabic());
    }

    bool operator<=(unsigned int b) {
        return this->arabic <= b;
    }

    bool operator>=(roman_number& b) {
        return this->operator>=(b.get_arabic());
    }

    bool operator>=(unsigned int b) {
        return this->arabic >= b;
    }

    std::string& get_roman() {
        return roman;
    }

    unsigned long get_arabic() {
        return arabic;
    }

    /**
        @brief Переводит римское число в арабское
        @param number - Римское число
        @return Арабское число
    */
    unsigned int calculate_arabic() {
        if (roman.length() == 0)
            return 0;

        std::vector<unsigned int> numbers;
        for (unsigned int i = 0; i < roman.length(); i++) {
            numbers.push_back(values[roman.at(i)]);
        }

        unsigned int result = 0;
        unsigned int num1 = 0;
        unsigned int num2 = 0;

        for (unsigned int i = 0; i < numbers.size() - 1; i++) {
            num1 = numbers.at(i);
            num2 = numbers.at(i + 1);
            if (num1 >= num2) {
                result += num1;
            } else {
                result -= num2;
            }
        }
        result += numbers.at(numbers.size() - 1);
        return result;
    }

    /**
        @brief Переводит арабское число в римское
        @param number - Арабское число
        @return Римское число
    */
    std::string calculate_roman() {
        if (arabic <= 0)
            return "";

        std::string result = "";
        std::string m = mill[arabic / 1000000];
        std::string th = thous_hunds[arabic / 100000 % 10];
        std::string tt = thous_ten[arabic / 10000 % 10];
        std::string t = thous[arabic / 1000 % 10];
        std::string h = hunds[arabic / 100 % 10];
        std::string te = tens[arabic / 10 % 10];
        std::string o = ones[arabic % 10];
        result = m + th + tt + t + h + te + o;
        return result;
    }

    friend std::ostream& operator<<(std::ostream&, roman_number&);
    friend std::istream& operator>>(std::istream&, roman_number&);
    friend roman_number operator+(roman_number&, roman_number&);
    friend roman_number operator+(roman_number&, unsigned int);
    friend roman_number operator-(roman_number&, roman_number&);
    friend roman_number operator-(roman_number&, unsigned int);
    friend roman_number operator*(roman_number&, roman_number&);
    friend roman_number operator*(roman_number&, unsigned int);
    friend roman_number operator/(roman_number&, roman_number&);
    friend roman_number operator/(roman_number&, unsigned int);
};

roman_number operator+(roman_number& a, roman_number& b) {
    return a + b.get_arabic();
}

roman_number operator+(roman_number& a, unsigned int b) {
    return roman_number(a.get_arabic() + b);
}

roman_number operator-(roman_number& a, roman_number& b) {
    return a - b.get_arabic();
}

roman_number operator-(roman_number& a, unsigned int b) {
    return roman_number(a.get_arabic() - b);
}

roman_number operator*(roman_number& a, roman_number& b) {
    return a * b.get_arabic();
}

roman_number operator*(roman_number& a, unsigned int b) {
    return roman_number(a.get_arabic() * b);
}

roman_number operator/(roman_number& a, roman_number& b) {
    return a / b.get_arabic();
}

roman_number operator/(roman_number& a, unsigned int b) {
    return roman_number(a.get_arabic() / b);
}

std::ostream& operator<<(std::ostream& os, roman_number& number) {
    os << number.get_roman() << " (" << number.get_arabic() << ")";
    return os;
}

std::istream& operator>>(std::istream& is, roman_number& number) {
    unsigned int value;
    roman_number temp("");
    std::string input;

    std::cout << "Do you want to enter arabic or roman value?\n  Type 1 or 2 => ";
    std::getline(is, input);

    if(input.empty() || !is || is.eof() || input.find_first_not_of("0123456789") != std::string::npos) {
        throw number_format_error();
    }

    value = std::stoul(input);

    if (value == 1) {
        std::cout << "Type arabic value => ";
        std::getline(is, input);

        if(input.empty() || !is || is.eof() || input.find_first_not_of("0123456789") != std::string::npos) {
            throw number_format_error();
        }

        temp = roman_number(std::stoul(input));
    } else {
        std::cout << "Type roman value => ";
        std::getline(is, input);

        if (input.empty() || !is || is.eof()) {
            throw roman_number_format_error();
        }

        check_roman_symbols(input);
        temp = roman_number(input);
    }
    number.from(temp);
    return is;
}

int main()
{
    values['M'] = 1000000;
    values['D'] = 500000;
    values['C'] = 100000;
    values['L'] = 50000;
    values['X'] = 10000;
    values['V'] = 5000;
    values['m'] = 1000;
    values['d'] = 500;
    values['c'] = 100;
    values['l'] = 50;
    values['x'] = 10;
    values['v'] = 5;
    values['i'] = 1;

    roman_number number("");
    std::cout << "---- Number #1 ----" << std::endl;
    std::cin >> number;

    roman_number number2("");
    std::cout << "---- Number #2 ----" << std::endl;
    std::cin >> number2;

    unsigned int choice = 10;
    unsigned int choice2 = 10;

    while (choice != 0) {
        std::cout << std::endl << "--- Menu ---" << std::endl << "'1' - Print arabic values" << std::endl << "'2' - Print roman values" << std::endl << "'3' - Print arithmetic operations" << std::endl << "'0' - Exit" << std::endl;
        std::cout << "=> ";
        std::cin >> choice;

        switch (choice) {
        case 0:
            std::cout << "Program terminated!" << std::endl;
            break;
        case 1:
            std::cout << "- Number (Arabic value) #1 " << number.get_arabic() << std::endl;
            std::cout << "- Number (Arabic value) #2 " << number2.get_arabic() << std::endl;
            break;
        case 2:
            std::cout << "- Number (Roman value) #1 " << number.get_roman() << std::endl;
            std::cout << "- Number (Roman value) #2 " << number2.get_roman() << std::endl;
            break;
        case 3:
            roman_number result(number);

            std::cout << "--- Arithmetic menu ---" << std::endl << "'1' - Subtraction" << std::endl << "'2' - Addition" << std::endl << "'3' - Multiplication" << std::endl << "'4' - Division" << std::endl;
            std::cout << "=> ";
            std::cin >> choice2;

            switch(choice2) {
            case 1:
                result = number - number2;
                std::cout << number << " - " << number2 << " = " << result << std::endl;
                break;
            case 2:
                result = number + number2;
                std::cout << number << " + " << number2 << " = " << result << std::endl;
                break;
            case 3:
                result = number * number2;
                std::cout << number << " * " << number2 << " = " << result << std::endl;
                break;
            case 4:
                result = number / number2;
                std::cout << number << " / " << number2 << " = " << result << std::endl;
                break;
            }
            break;
        }
    }

    return 0;
}
