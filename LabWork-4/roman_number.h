#ifndef ROMAN_NUMBER_H
#define ROMAN_NUMBER_H

#include <iostream>
#include <map>

/** Класс, описывающий римское число */
class roman_number {
    /// Переменная, хранящая римское представление числа
    std::string roman;
    /// Переменная, хранящая арабское представление числа
    unsigned int arabic;

public:
    /// Статичные переменные, используемые для перевода чисел римское <=> арабское
    static std::string ones[10];
    static std::string tens[10];
    static std::string hunds[10];
    static std::string thous[10];
    static std::string thous_ten[10];
    static std::string thous_hunds[10];
    static std::string mill[10];

    /// Список символов римских чисел и их значение в арабской системе исчисления
    static std::map<char, unsigned int> values;

    /// Статичный метод, проверяющий переданную строку на валидность в римской системе исчисления
    static bool check_roman_symbols(std::string&);

    /// Конструктор, создающий римское число из строки
    roman_number(std::string = "");
    /// Конструктор, создающий римское число из арабского
    roman_number(unsigned long = 0);
    /// Копирующий конструктор
    roman_number(roman_number*);

    /// Метод, копирующий данные в это римское число из переданного
    void from(roman_number&);

    /// Прибавляет к этому римскому числу значение переданного
    roman_number& add(roman_number&);
    /// Отнимает от этого римского числа значение переданного
    roman_number& subtract(roman_number&);
    /// Умножает это римское число на значение переданного
    roman_number& multiply(roman_number&);
    /// Делит это римское число на значение переданного
    roman_number& divide(roman_number&);

    /// Арифметические операторы и операторы присваивания
    roman_number& operator=(std::string);
    roman_number& operator=(unsigned int);
    roman_number& operator+=(roman_number&);
    roman_number& operator+=(unsigned int);
    roman_number& operator-=(roman_number&);
    roman_number& operator-=(unsigned int);
    roman_number& operator*=(roman_number&);
    roman_number& operator*=(unsigned int);
    roman_number& operator/=(roman_number&);
    roman_number& operator/=(unsigned int);

    /// Инктрименты/Декрименты
    roman_number operator++();
    roman_number operator++(int);
    roman_number operator--();
    roman_number operator--(int);

    /// Операторы сравнения
    bool operator==(roman_number&);
    bool operator==(unsigned int);
    bool operator!=(roman_number&);
    bool operator!=(unsigned int);
    bool operator<(roman_number&);
    bool operator<(unsigned int);
    bool operator>(roman_number&);
    bool operator>(unsigned int);
    bool operator<=(roman_number&);
    bool operator<=(unsigned int);
    bool operator>=(roman_number&);
    bool operator>=(unsigned int);

    /// Возвращает значение в римской системе исчисления
    std::string& get_roman();
    /// Возвращает значение в арабской системе исчисления
    unsigned int get_arabic();

    /// Высчитывает и возвращает арабское значение исходя из римского, записанного в этом объекте
    unsigned int calculate_arabic();
    /// Высчитывает и возвращает римское значение исходя из арабского, записанного в этом объекте
    std::string calculate_roman();

    /// Потоковые операторы
    friend std::ostream& operator<<(std::ostream&, roman_number&);
    friend std::istream& operator>>(std::istream&, roman_number&);

    /// Арифметические операторы
    friend roman_number operator+(roman_number&, roman_number&);
    friend roman_number operator+(roman_number&, unsigned int);
    friend roman_number operator-(roman_number&, roman_number&);
    friend roman_number operator-(roman_number&, unsigned int);
    friend roman_number operator*(roman_number&, roman_number&);
    friend roman_number operator*(roman_number&, unsigned int);
    friend roman_number operator/(roman_number&, roman_number&);
    friend roman_number operator/(roman_number&, unsigned int);
};

#endif
