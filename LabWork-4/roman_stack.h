#ifndef ROMAN_STACK_H
#define ROMAN_STACK_H

#include "roman_number.h"
#include <stack>

/** Класс, описывающий стэк римских чисел */
class roman_stack
{
    /// Внутренний стэк из стандартных библиотек С++
    std::stack<roman_number> stack;

public:
    /// Конструтор, создающий пустой стэк
    roman_stack();
    /// Копирующий конструтор
    roman_stack(roman_stack&);

    /// Добавление римского числа по его классу
    void push(roman_number&);

    /// Добавление римского числа по его арабскому значению
    void push(unsigned int);

    /// Добавление римского числа по его текстовому представлению
    void push(std::string&);

    /// Удаляет верхний элемент стэка
    void pop();

    /// Возвращает верхний элемент стэка
    roman_number& top();

    /// Вовзращает размер стэка
    unsigned int size();

    /// Проверяет стэк на отсутствие элементов
    bool empty();
};

#endif
