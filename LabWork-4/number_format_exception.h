#ifndef NUMBER_FORMAT_ERROR_H
#define NUMBER_FORMAT_ERROR_H

#include <iostream>

/** Исключение, выбрасываемое при некорректном вводе числа */
class number_format_exception : public std::exception {
public:
    const char * what() const noexcept;
};

#endif
