#ifndef ROMAN_NUMBER_FORMAT_ERROR_H
#define ROMAN_NUMBER_FORMAT_ERROR_H

#include <iostream>

/** Исключение, выбрасываемое при некорректном вводе римского числа */
class roman_number_format_exception : public std::exception {
public:
    const char * what() const noexcept;
};

#endif
