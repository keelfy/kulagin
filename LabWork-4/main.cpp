#include <iostream>
#include <random>
#include <time.h>
#include <sstream>
#include "roman_stack.h"

/// Суммирует все римские числа из стэка, и записывает сумму в конец этого же стэка
void sum_up_stack(roman_stack& stack) {
    roman_stack buffer(stack); // Создаем новый стэк, в который копируем стэк из аргументов
    unsigned int sum = 0; // Переменная суммы = 0

    for (unsigned int i = 0; i < stack.size(); i++) { // Цикл по элементам стэка
        sum += buffer.top().get_arabic(); // Суммируем арабские значения
        buffer.pop(); // Удаляем верхний элемент из стэка-буффера
    }

    stack.push(sum); // Записываем сумму последним элементом в стэк из аргументов
};

int main()
{
    srand(time_t(0));

    roman_stack stack1;
    roman_stack stack2;

    unsigned int value;

    std::stringstream ss1;
    std::stringstream ss2;

    for (unsigned int i = 0; i < 10; i++) {
        value = rand() % 3 + 1;
        stack1.push(value);
        ss1 << value << " ";

        value = rand() % 3 + 1;
        stack2.push(value);
        ss2 << value << " ";
    }

    std::cout << "Stack #1 => " << ss1.str() << std::endl;
    std::cout << "Stack #2 => " << ss2.str() << std::endl;

    sum_up_stack(stack1);

    stack2.push(stack1.top());
    sum_up_stack(stack2);

    std::cout << "Sum of two stacks => " << stack2.top() << std::endl;

    return 0;
};
